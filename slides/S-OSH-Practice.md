---
title: Open Source Hardware | Real Life Voices (OSH at SRH 05-11)
type: slide
slideOptions:
  transition: slide
...

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

## Open Source Hardware
## - Real Life Voices - 
---

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/oer-osh-practice/-/blob/master/slides/S-OSH-Practice.md)

<!--- 240min in total -->

---

# Intro
---

Facing Reengineering

    "If your idea can be sold [by others], it will be."
    – Nathan Seidle, Sparkfun

---

<!-- .slide: data-background="https://cdn.sparkfun.com/assets/e/1/7/1/6/50894d29ce395f1038000001.png" data-background-color="#000" -->

---

## Sparkfun's reality
---

- <span>reengineering will happen no matter what<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>average of <!-- .element: class="fragment" data-fragment-index="2" --></span><span>**3 months** <!-- .element: class="fragment" data-fragment-index="2" --></span><span>(after product launch) until customers can get the copy from China<!-- .element: class="fragment" data-fragment-index="2" --></span>

--

<span>→ So why not doing it open source in the first place?<!-- .element: class="fragment" data-fragment-index="3" --></span>

---

<iframe width="1024" height="576" data-src="https://www.youtube.com/embed/aglidIqSrnE?start=395" allowfullscreen data-autoplay></iframe>

---

#### qualify by competence, rather than dependency
---

- special knowledge, 
- special manufacturing techniques,
- faster innovation
- better support
- […]

…under a powerful brand if you fancy
 

Note:
    Sparkfun was a pioneer for that (and became part of the board of OSHWA)

---

# Timm's Homework
---

---

## Schedule
---

1. <span>Role Playing General Misconceptions<!-- .element: class="fragment" data-fragment-index="1" --></span>
    1. <span>Licensing<!-- .element: class="fragment" data-fragment-index="1" --></span>
    2. <span>Version Control<!-- .element: class="fragment" data-fragment-index="1" --></span>
2. <span>Welcome to the Real World<!-- .element: class="fragment" data-fragment-index="2" --></span>
    1. <span>DIN | Amelie<!-- .element: class="fragment" data-fragment-index="2" --></span>
    2. <span>OSH Spectrometer | Alessandro<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

# Role Playing
---

### Licensing

---

### Round 1: Proprietary
---

We need:

- project owner
- lawyer
- user

Note:
    step up or Jishnu will choose for you
    @roles please turn on your camera

---

idea → lawyer → license → license fees

    1. You pay the lawyer
    2. User pays you (incl. lawyer costs)

---

![](https://wiki.q-researchsoftware.com/images/1/17/Installation_accept_license.png)

---

### Round 2: Open Source
---

We need:

- project owner
- lawyer
- user

---

idea → license chooser → license → rock'n'roll

    nobody pays
    no lawyer invovled

---

<iframe width="1024" height="576" data-src="https://choosealicense.com/"></iframe>

---

# Q&A
---

<span>What about contracts and other documents?<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    OSEG's legal issues group is planning a project to create standard contract templates etc.

---

# Role Playing
---

### Version Control

local vs. central vs. decentral

---

### Round 1: local
---

We need:

- Maintainer
- Contributor

---

|   |   |
| -------- | -------- |
| Maintainer     | takes this large complex document     |
| Contributor     | makes changes     |

---

- `Maintainer`:
    - <span>takes <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[this large complex document](https://gitlab.com/osh-academy/oer-osh-practice/-/blob/master/lecture-actions/L-role-play/L-VersionControl-sample1.ods)<!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>passes it on to <!-- .element: class="fragment" data-fragment-index="2" --></span><span>`Contributor`<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Contributor`:
    - <span>makes modifications<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>returns document to <!-- .element: class="fragment" data-fragment-index="4" --></span><span>`Maintainer`<!-- .element: class="fragment" data-fragment-index="4" --></span>
- `Maintainer`:
    - <span>What changes have been made?<!-- .element: class="fragment" data-fragment-index="5" --></span>

---

<iframe width="1024" height="576" data-src="http://ww3.cad.de/foren/ubb/uploads/Christian.O/Uebung_02_Kurbelzapfen_Antrieb.CATDrawing.pdf"></iframe>

---

## Discussion
---

- <span>Documentation on USB Sticks<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>'track changes' in text documents<!-- .element: class="fragment" data-fragment-index="2" --></span>

<span>![](https://wiki.documentfoundation.org/images/2/22/Track-changes-Word-2010.jpg)<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

### Round 2: central
---

We need:

- Contributor 1
- Contributor 2
- Central Server

---

- `Central Server`:
    - <span>holds <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[this large complex document](https://gitlab.com/osh-academy/oer-osh-practice/-/blob/master/lecture-actions/L-role-play/L-VersionControl-sample1.ods)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Contributor 1`:
    - <spanrequest access to document at <!-- .element: class="fragment" data-fragment-index="2" --></span><span>`Central Server`<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Central Server`:
    - <span>grant access to <!-- .element: class="fragment" data-fragment-index="3" --></span><span>`Contributor 1` <!-- .element: class="fragment" data-fragment-index="3" --></span><span>(check-in)<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>log changes<!-- .element: class="fragment" data-fragment-index="4" --></span>

…

---

…

- `Contributor 2`:
    - <span>request document at <!-- .element: class="fragment" data-fragment-index="1" --></span><span>`Central Server`<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Central Server`:
    - <span>access denied, file is locked<!-- .element: class="fragment" data-fragment-index="2" --></span>
    - <span>Why? → Avoid conflicts!<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Contributor 1`:
    - <span>done (check-out)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- `Central Server`:
    - <span>coolsies, see you, pal<!-- .element: class="fragment" data-fragment-index="4" --></span>

---

## Discussion
---

- <span>What about Clouds?<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>What about HedgeDoc, Onlyoffice, GoogleDocs […]?<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

### Round 3: decentral
---

We need:

- Contributor 1
- Contributor 2

---

- `Contributor 1`:
    - <span>forks <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[this large complex document](https://gitlab.com/osh-academy/oer-osh-practice/-/blob/master/lecture-actions/L-role-play/L-VersionControl-sample3.csv)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Contributor 2`:
    - <span>forks the fork, makes changes<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Contributor 1`:
    - <span>makes changes<!-- .element: class="fragment" data-fragment-index="3" --></span>
- `Contributor 2`:
    - <span>files 'merge request' to <!-- .element: class="fragment" data-fragment-index="4" --></span><span>`Contributor 1`<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>`git:`<!-- .element: class="fragment" data-fragment-index="5" --></span>
    - <span>automatic merging (where possible)<!-- .element: class="fragment" data-fragment-index="5" --></span>
    - <span>asks <!-- .element: class="fragment" data-fragment-index="6" --></span><span>`Contributor 1` <!-- .element: class="fragment" data-fragment-index="6" --></span><span>to clarify unresolvable conflicts<!-- .element: class="fragment" data-fragment-index="6" --></span>

---

## Discussion
---

- What about binaries?

---

# Q&A
---

<span>How about other websites like Wikifactory or Mediawiki-based sites?<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

Meet the real world

after a **25min**

# Bio Break

---

<iframe width="1024" height="576" data-src="https://vclock.com/timer/#countdown=00:25:00&enabled=0&seconds=1500&title=cya+in%3A&sound=xylophone&loop=0"></iframe>

---

# DIN
---

How standards shape our economy

<span>Welcome Amelie :)<!-- .element: class="fragment" data-fragment-index="1" --></span>

    #guerillapolitics

---

# DIN SPEC 3105
---

`OSH Standardisation-SRH-201126-tattered.odp`

- add process and consortium

---

# Q&A
---

---

# PS Lab Spectrometer
---

Let's actually solve the problem.

<span>Welcome Alessandro :)<!-- .element: class="fragment" data-fragment-index="1" --></span>

    #openscience

---

# Outro
---

Note:
    - be the change you want to see in this world
    - there are plenty of places to start


---

<!-- .slide: data-background="https://hackaday.com/wp-content/uploads/2020/10/libre_solar_box.jpg?w=800
" data-background-color="#000" -->

# LibreSolar

[**Open Sourcing Solar Power**](https://libre.solar/)

---

<!-- .slide: data-background="https://raw.githubusercontent.com/opencultureagency/ASKotec/master/thumbs/thumb-2.jpg" data-background-color="#000" -->

# r0g_agency

[**Access to Skills and Knowledge Network**](https://openculture.agency/asknet-access-to-skills-and-knowledge-network/)

---

<!-- .slide: data-background="https://www.cadus.org/assets/images/a/18954818_847880305366669_5621617425344554314_o-bc68608f.webp" data-background-color="#000" -->

# CADUS

[**crisis response**](https://www.cadus.org/en/)

---

<!-- .slide: data-background="https://talks.rc3.oio.social/media/r3c-oio/images/WSU7ZL/Bildschirmfoto_2020-12-23_um_19.04.15_CHpkBue.png" data-background-color="#000" -->

---

<!-- .slide: data-background="https://rwb27.github.io/openflexure_microscope/docs/images/microscope_gosh.jpg" data-background-color="#000" -->

# OpenFlexure

[**3D-printed, remote-controllable microscope**](https://openflexure.org/)

---

<!-- .slide: data-background="https://www.crowdsupply.com/img/9cdd/20200422-reform-hero_jpg_project-body.jpg" data-background-color="#000" -->

# MNT Research

[**fully open source notebook**](https://mntre.com/)

---

Have a wonderful week

and

# thank you

for contributing to a better world for everyone
